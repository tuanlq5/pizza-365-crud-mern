import axios from "axios";
import { ORDER_MENU_SIZE_CHANGE, 
    ORDER_PIZZA_TYPE_CHANGE, 
    ORDER_DRINK_CHANGE, 
    ORDER_CLIENT_FULLNAME_CHANGE,
    ORDER_CLIENT_EMAIL_CHANGE,
    ORDER_CLIENT_ADDRESS_CHANGE,
    ORDER_CLIENT_PHONENUMBER_CHANGE,
    ORDER_CLIENT_VOUCHER_CHANGE,
    ORDER_CLIENT_MESSAGE_CHANGE,
    ORDER_CHECK_DATA,
    CHECK_VOUCHER_SUCCESS,
    SEND_ORDER_START,
    SEND_ORDER_SUCCESS,
    SEND_ORDER_ERROR,
    CLOSE_MODAL,
} from "../constants/order.constant";
// Action receive data from client action
const orderMenuSizeChange = (value) => {
    return{
        type: ORDER_MENU_SIZE_CHANGE,
        payload: value
    }
}
// ACtion Pizza khi được chọn
const orderPizzaTypeChange = (value) => {
    return{
        type: ORDER_PIZZA_TYPE_CHANGE,
        payload: value
    }
}
//Action Drink khi được select
const orderDrinkChange = (value) => {
    return{
        type: ORDER_DRINK_CHANGE,
        payload: value
    }
} 
//Action tên khách hàng khi được nhập
const orderFullNameChange = (value) => {
    return{
        type: ORDER_CLIENT_FULLNAME_CHANGE,
        payload: value
    }
}
//Action email khi được nhập
const orderEmailChange = (value) => {
    return{
        type: ORDER_CLIENT_EMAIL_CHANGE,
        payload: value
    }
}
//Action Số điện thoại khi được nhập
const orderPhoneNumberChange = (value) => {
    return{
        type: ORDER_CLIENT_PHONENUMBER_CHANGE,
        payload: value
    }
}
//Action Địa chỉ khi được nhập
const orderAddressChange = (value) => {
    return{
        type: ORDER_CLIENT_ADDRESS_CHANGE,
        payload: value
    }
}
//Action Voucher khi được nhập
const orderVoucherChange = (value) => {
    return{
        type: ORDER_CLIENT_VOUCHER_CHANGE,
        payload: value
    }
}
//Action tin nhắn khi được nhập
const orderMessageChange = (value) => {
    return{
        type: ORDER_CLIENT_MESSAGE_CHANGE,
        payload: value
    }
}
//Action khi nút kiểm tra đơn hàng được ân
const checkData = () => {
    return{
        type: ORDER_CHECK_DATA,
    }
}
//Redux thunk Check Voucher is valid or not
// const checkVoucher  = (voucher) => async (dispatch) => {
//     // return async dispatch => {
//     // dispatch({ type: CHECK_VOUCHER_START});
//     // await axios({
//     //     method: "get",
//     //     url: `http://localhost:8000/devcamp-voucher-api/voucher_detail/${voucher}`
//     // })
//     //     .then(data => {
//     //         dispatch({type: CHECK_VOUCHER_SUCCESS, payload: data.data});
//     //     })
//     //     .catch(error => {
//     //         dispatch({ type: CHECK_VOUCHER_ERROR, error})
//     //     })
//     // }
//     try {
//         dispatch({type: CHECK_VOUCHER_START});

//         const response = await axios.get(`http://localhost:8000/devcamp-voucher-api/voucher_detail/${voucher}`);
//         const discountPercent = response.data.data;
        
//         // Dispatch an action to update the state with the discount percentage
//         dispatch({ type: MAKE_SURE_VOUCHER_SEND , payload: discountPercent });
    
//         // Dispatch the next action to apply the voucher with the discount percentage
//         dispatch({ type: CHECK_VOUCHER_SUCCESS, payload: voucher });
//       } catch (error) {
//         // Dispatch an action to handle errors
//         dispatch({ type: CHECK_VOUCHER_ERROR , payload: error.message });
//     }
// }
//Make sure discount send back
// const discountReceive = () => async(dispatch, getState) => {
//     //Get Data From state
//     const myData = getState().myData;
//     //Dispatch action to update state with next data
//     dispatch({type: MAKE_SURE_VOUCHER_SEND})
// }
//Redux Thunk Send Order
const  applyVoucher  = (data) =>{
    return {
        type: CHECK_VOUCHER_SUCCESS,
        payload: data
    }
}
const sendOrder  = (data) => {
    return dispatch => {
    dispatch({ type: SEND_ORDER_START});
    axios({
        method: "post",
        url: `http://localhost:8000/devcamp-pizza365/orders`,
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
          },
    })
        .then(data => {
            dispatch({type: SEND_ORDER_SUCCESS, payload: data.data});
        })
        .catch(error => {
            dispatch({ type: SEND_ORDER_ERROR, error});
            console.log(error.response);
        })
    }
}
//Action Close the modal
const closeModal = () => {
    return{
        type: CLOSE_MODAL,
    }
}
export{
    orderMenuSizeChange,
    orderPizzaTypeChange,
    orderDrinkChange,
    orderFullNameChange,
    orderEmailChange,
    orderPhoneNumberChange,
    orderAddressChange,
    orderVoucherChange,
    orderMessageChange,
    checkData,
    applyVoucher,
    sendOrder,
    closeModal
}