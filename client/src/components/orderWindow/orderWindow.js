import { Grid, Button} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import SendIcon from '@mui/icons-material/Send';
import { sendOrder } from "../../action/order.action";
import "./orderWindow.css";
export default function OrderWindow (){
    //Khởi tạo Dispath
    const dispatch = useDispatch();
    //lấy thông tin từ store
    const showOrderWindow = useSelector(state => state.orderReducer.showWindow)
    const ORDER = useSelector (state => state.orderReducer.orderContainer);
    const OrderSend = useSelector(state => state.orderReducer.orderSend);
    const discount = useSelector(state => state.orderReducer.phanTramGiamGia);
    //Khai báo action
    const SendOrderToAPI = () => {
        console.log(OrderSend)
        dispatch(sendOrder(OrderSend));
    }
    return (
        <>
        {(showOrderWindow ) ? 
             <Grid className="Check-Order show" container mb={4} sx={{backgroundColor: "cyan", borderRadius:"6px"}}>
                <Grid item md={12} xs={12} sx={{display:"flex", justifyContent:"center", marginTop:"15px"}}>
                    <p style={{fontWeight: "500", fontSize: "30px", marginBottom:"0px"}}>Chi Tiết Đơn Hàng</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{display:"flex", justifyContent:"center"}}>
                    <p >------------------------</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Combo size: {ORDER.menuSize.size}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Đường kính pizza: {ORDER.menuSize.duongKinh}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Sường nướng: {ORDER.menuSize.suon}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Salad: {ORDER.menuSize.salad}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Số lượng đồ uống: {ORDER.menuSize.soLuongNuoc}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Đơn giá: {ORDER.menuSize.donGia} VND</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Loại Pizza: {ORDER.loaiPizza}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Loại đồ uống: {ORDER.loaiDoUong}</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{display:"flex", justifyContent:"center", marginTop:"10px", marginBottom:"0px"}}>
                    <p style={{fontWeight: "500", fontSize: "30px", marginBottom:"0px"}}>Thông Tin Khách Hàng</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{display:"flex", justifyContent:"center"}}>
                    <p >------------------------</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Họ và tên: {ORDER.hoVaTen}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Email: {ORDER.email} </p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Số Điện Thoại: {ORDER.soDienThoai}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Địa Chỉ: {ORDER.diaChi}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "0px"}}>Mã Giảm Giá: {ORDER.maGiamGia}</p>
                </Grid>
                <Grid item md={6} xs={6} sx={{padding: "7px 170px"}}>
                    <p style={{marginBottom: "10px"}}>Lời Nhắn: {ORDER.loiNhan}</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{padding: "10px 0 10px 0", display: "flex", justifyContent:"center"}}>
                    <p style={{marginBottom: "10px", fontSize:"20px"}}>Phần trăm giảm giá: {discount} %</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{padding: "7px 30px", display: "flex",  justifyContent:"center !important"}}>
                    <p style={{marginBottom: "10px", fontSize:"20px"}}>Số tiền khách hàng cần thanh toán: {OrderSend.thanhTien} VND</p>
                </Grid>
                <Grid item md={12} xs={12} sx={{padding: "10px 0 30px 0", display: "flex", justifyContent:"center"}}>
                    <Button variant="contained" endIcon={<SendIcon />} sx={{backgroundColor:"Orange", width:"200px"}} onClick={SendOrderToAPI}> Send Order</Button>
                </Grid>
            </Grid>
        : 
            <></>
            }
        </>
    )
}