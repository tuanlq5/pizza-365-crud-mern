import { 
    ORDER_MENU_SIZE_CHANGE, 
    ORDER_PIZZA_TYPE_CHANGE, 
    ORDER_DRINK_CHANGE, 
    ORDER_CLIENT_FULLNAME_CHANGE,
    ORDER_CLIENT_EMAIL_CHANGE,
    ORDER_CLIENT_ADDRESS_CHANGE,
    ORDER_CLIENT_PHONENUMBER_CHANGE,
    ORDER_CLIENT_VOUCHER_CHANGE,
    ORDER_CLIENT_MESSAGE_CHANGE,
    CHECK_VOUCHER_SUCCESS,
    ORDER_CHECK_DATA,
    SEND_ORDER_START,
    SEND_ORDER_SUCCESS,
    SEND_ORDER_ERROR,
    CLOSE_MODAL,
} from "../constants/order.constant";
//Global State
const initialState = {
    orderContainer:{
        menuSize: {
            size: "",
            duongKinh: "" ,
            suon: -1,
            salad: "",
            soLuongNuoc: -1,
            donGia: -1,
        },
        loaiPizza: "",
        loaiDoUong: "",
        hoVaTen:"",
        email:"",
        soDienThoai:"",
        diaChi:"",
        maGiamGia:"",
        loiNhan:"",
        tienThanhToan: -1
    },
    maGiamGia:"",
    phanTramGiamGia: 0,
    voucherLoading: false,
    showWindow: false,
    orderSend: {
            kichCo: "",
            duongKinh: "",
            suon:-1 ,
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        },
    orderID : "",
    modal: false,
}
//hàm validate order
const validateOrder = (param) => {
    if(param.menuSize.size === "") {
        alert("Bạn chưa chọn Combo!");
        return false;
    }if(param.loaiPizza === "") {
        alert("Bạn chưa chọn Pizza!");
        return false;
    }if(param.loaiDoUong === "") {
        alert("Bạn chưa chọn đồ uống!");
        return false;
    }if(param.hoVaTen === "") {
        alert("Bạn chưa nhập họ và tên!");
        return false;
    }if(param.email === "") {
        alert("Bạn chưa nhập email!");
        return false;
    }if(param.soDienThoai === "") {
        alert("Bạn chưa nhập số điện thoại!");
        return false;
    }if(param.menuSize.size === "") {
        alert("Bạn chưa nhập địa chỉ!");
        return false;
    }if( param.email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) == null){
        alert("Email không đúng cú pháp!");
        return false;
    }if( param.soDienThoai.match(/(84|0[3|5|7|8|9])+([0-9]{8})\b/) == null){
        alert("Số điện thoại chưa đúng!");
        return false;
    }
    return true;
}

const orderReducer = (state = initialState, action)=> {
    switch (action.type){
        //Reducer Lấy thông tin combo
        case ORDER_MENU_SIZE_CHANGE:
            state.orderContainer.menuSize.size = action.payload;
            // console.log(state.orderContainer);
            //thêm thông tin vào trường menuSize khi có thông tin size {"S", "M", "L"}
            if(state.orderContainer.menuSize.size === "S"){
                state.orderContainer.menuSize.duongKinh = "20cm";
                state.orderContainer.menuSize.suon = 2;
                state.orderContainer.menuSize.salad = "200g";
                state.orderContainer.menuSize.soLuongNuoc = 2;
                state.orderContainer.menuSize.donGia = 150000;
                // console.log(state.orderContainer);
            }
            if(state.orderContainer.menuSize.size === "M"){
                state.orderContainer.menuSize.duongKinh = "25cm";
                state.orderContainer.menuSize.suon = 4;
                state.orderContainer.menuSize.salad = "300g";
                state.orderContainer.menuSize.soLuongNuoc = 3;
                state.orderContainer.menuSize.donGia = 200000;
                // console.log(state.orderContainer);
            }
            if(state.orderContainer.menuSize.size === "L"){
                state.orderContainer.menuSize.duongKinh = "30cm";
                state.orderContainer.menuSize.suon = 8;
                state.orderContainer.menuSize.salad = "500g";
                state.orderContainer.menuSize.soLuongNuoc = 4;
                state.orderContainer.menuSize.donGia = 250000;
                // console.log(state.orderContainer);
            }
            break;
        //Reducer Lấy thông tin Pizza 
        case ORDER_PIZZA_TYPE_CHANGE: 
            state.orderContainer.loaiPizza = action.payload;
            break;
        //Reducer Lấy thông tin đồ uống
        case ORDER_DRINK_CHANGE:
            state.orderContainer.loaiDoUong = action.payload;
            break;
        //Reducer lấy thông tin tên người đặt order
        case ORDER_CLIENT_FULLNAME_CHANGE:
            state.orderContainer.hoVaTen = action.payload;
            break;
        //Reducer Lấy thông tin Email
        case ORDER_CLIENT_EMAIL_CHANGE:
            state.orderContainer.email = action.payload;
            break;
        //Reducer lấy thông tin số điện thoại
        case ORDER_CLIENT_PHONENUMBER_CHANGE:
            state.orderContainer.soDienThoai = action.payload;
            break;
        //Reducer lấy thông tin địa chỉ
        case ORDER_CLIENT_ADDRESS_CHANGE:
            state.orderContainer.diaChi = action.payload;
            break;
        //Reducer lấy thông tin voucher
        case ORDER_CLIENT_VOUCHER_CHANGE:
            state.orderContainer.maGiamGia = action.payload;
            state.maGiamGia = action.payload;
            break;
        //Reducer lấy thông tin lời nhắn
        case ORDER_CLIENT_MESSAGE_CHANGE:
            state.orderContainer.loiNhan = action.payload;
            break;
        //Reducer khi voucher được trả về
        case CHECK_VOUCHER_SUCCESS:
            state.phanTramGiamGia = action.payload[0].phanTramGiamGia;
            console.log(action.payload[0].phanTramGiamGia);
            console.log(`Phần trăm giảm giá ${state.phanTramGiamGia}%`);
            state.voucherLoading = true;
            break;
        //Reducer khi User ấn nút kiểm tra đơn hàng
        case ORDER_CHECK_DATA:
            if(validateOrder(state.orderContainer)){
                //thay đổi giá khi áp dụng voucher 
                state.orderContainer.tienThanhToan = state.orderContainer.menuSize.donGia * (100 - state.phanTramGiamGia)/100;
                console.log(state.orderContainer); 
                state.showWindow = true;
                state.orderContainer.menuSize.donGia = state.orderContainer.menuSize.donGia.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                state.orderSend.kichCo = state.orderContainer.menuSize.size;
                state.orderSend.duongKinh = state.orderContainer.menuSize.duongKinh;
                state.orderSend.suon = state.orderContainer.menuSize.suon;
                state.orderSend.soLuongNuoc = state.orderContainer.menuSize.soLuongNuoc;
                state.orderSend.salad = state.orderContainer.menuSize.salad;
                state.orderSend.loaiPizza = state.orderContainer.loaiPizza;
                state.orderSend.idVourcher = state.orderContainer.maGiamGia;
                state.orderSend.idLoaiNuocUong = state.orderContainer.loaiDoUong;
                state.orderSend.hoTen = state.orderContainer.hoVaTen;
                state.orderSend.thanhTien = state.orderContainer.tienThanhToan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                console.log(state.orderSend.thanhTien);
                state.orderSend.email = state.orderContainer.email;
                state.orderSend.soDienThoai = state.orderContainer.soDienThoai;
                state.orderSend.diaChi = state.orderContainer.diaChi;
                state.orderSend.loiNhan = state.orderContainer.loiNhan;
            } else {
                state.showWindow = false;
            }
            break;
        //Reducer khi gửi Order
        case SEND_ORDER_START:
            break;
        //Reducer Khi Order gửi thàn công
        case SEND_ORDER_SUCCESS:
            //Thêm data vào order code
            state.orderID = action.payload.data.orderCode;
            //Hiển thị modal
            state.modal = true;
            console.log(action.payload);
            console.log(state.orderID);
            //Xóa dữ liệu order
            state.orderSend.kichCo = "";
            state.orderSend.duongKinh = "";
            state.orderSend.suon = 0;
            state.orderSend.soLuongNuoc = 0;
            state.orderSend.salad = "";
            state.orderSend.loaiPizza = "";
            state.orderSend.idVourcher = "";
            state.orderSend.idLoaiNuocUong = "";
            state.orderSend.hoTen = "";
            state.orderSend.thanhTien = 0;
            state.orderSend.email = "";
            state.orderSend.soDienThoai = "";
            state.orderSend.diaChi = "";
            state.orderSend.loiNhan = "";
            // state.orderContainer.menuSize.size = {
            //     size: "",
            //     duongKinh: "" ,
            //     suon: -1,
            //     salad: "",
            //     soLuongNuoc: -1,
            //     donGia: -1,
            // };
            // state.orderContainer.loaiPizza = "";
            // state.orderContainer.maGiamGia = " ";
            // state.orderContainer.loaiDoUong = "";
            // state.orderContainer.hoVaTen = "";
            // state.orderContainer.tienThanhToan = "";
            // state.orderContainer.email = "";
            // state.orderContainer.soDienThoai = "";
            // state.orderContainer.diaChi = "";
            // state.orderContainer.loiNhan = "";
            break;
        //Reducer khi order không chuyển thành công
        case SEND_ORDER_ERROR:
            break;
        //Reducer đóng modal
        case CLOSE_MODAL:
            state.modal = false;
            state.showWindow = false;
            break;
    default:
        break;
    }
    return {...state};
}
export default orderReducer;