//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");
const createOrder = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "kichCo": "M",
    //     "duongKinh": "25",
    //     "suon": "4",
    //     "salad": "300",
    //     "loaiPizza": "HAWAII",
    //     "idVourcher": "16512",
    //     "idLoaiNuocUong": "PEPSI",
    //     "soLuongNuoc": "3",
    //     "hoTen": "Phạm Thanh Bình",
    //     "thanhTien": "200000",
    //     "email": "binhpt001@devcamp.edu.vn",
    //     "soDienThoai": "0865241654",
    //     "diaChi": "Hà Nội",
    //     "loiNhan": "Pizza đế dày"
    // }
    // Bước 2: validate dữ liệu
    if(!body.kichCo){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: `kich cỡ không hợp lệ: ${body.kichCo}`
        })
    }
    if(!body.duongKinh){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Đường kính không hợp lệ" 
        })
    }
    if(!body.suon){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Sườn không hợp lệ" 
        })
    }
    if(!body.salad){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Salad không hợp lệ" 
        })
    }
    if(!body.loaiPizza){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Loại Pizza không hợp lệ" 
        })
    }
    if(!body.idLoaiNuocUong){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Loại đồ uống không hợp lệ" 
        })
    }
    if(!body.soLuongNuoc){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Sô lượng nước không hợp lệ" 
        })
    }
    if(!body.hoTen){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Họ và tên không hợp lệ" 
        })
    }
    if(!body.thanhTien){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Thành tiền không hợp lệ" 
        })
    }
    if(!body.email){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    }
    if(!body.soDienThoai){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Số điện thoại không hợp lệ" 
        })
    }
    if(!body.soDienThoai){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Địa chỉ không hợp lệ" 
        })
    }
    // bước 3: Gọi Model tạo dữ liệu
    const newOrder = {
        kichCo: body.kichCo,
        duongKinh: body.duongKinh,
        suon: body.suon,
        salad: body.salad,
        loaiPizza: body.loaiPizza,
        idVourcher: body.idVourcher,
        idLoaiNuocUong: body.idLoaiNuocUong,
        soLuongNuoc: body.soLuongNuoc,
        hoTen: body.hoTen,
        thanhTien: body.thanhTien,
        email: body.email,
        soDienThoai: body.soDienThoai,
        diaChi: body.diaChi,
        loiNhan: body.loiNhan
    };
    //bước 4: trả về kết quả 
    orderModel.create(newOrder, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create Order: Successfull",
            data: data
        })
    } );
}
const getAllOrder  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    orderModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all order: Successfull",
            data: data
        })
    })
}
const getOrderByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const orderID =  request.params.orderId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderID)){
     return response.status(400).json({
             status: "Bad request",
             message: "Order Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     orderModel.findById(orderID,(error,data) =>{
         return response.status(200).json({
             status: "Get Detail Order successfull",
             data: data
             })
         }
     )  
 }
 const updateOrderByID = (request,response) => {
    // bước 1: chuẩn bị dữ liệu
    const orderId =  request.params.orderId;
    const body = request.body;

   //bước 2: Validate dữ liệu
   if(!body.kichCo){
    return response.status(400).json({
        status: "BAD REQUEST",
        message: "kich cỡ không hợp lệ" 
    })
    }
    if(!body.duongKinh){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Đường kính không hợp lệ" 
        })
    }
    if(!body.suon){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Sườn không hợp lệ" 
        })
    }
    if(!body.salad){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Salad không hợp lệ" 
        })
    }
    if(!body.loaiPizza){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Loại Pizza không hợp lệ" 
        })
    }
    if(!body.idLoaiNuocUong){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Loại đồ uống không hợp lệ" 
        })
    }
    if(!body.soLuongNuoc){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Sô lượng nước không hợp lệ" 
        })
    }
    if(!body.hoTen){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Họ và tên không hợp lệ" 
        })
    }
    if(!body.thanhTien){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Thành tiền không hợp lệ" 
        })
    }
    if(!body.email){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    }
    if(!body.soDienThoai){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Số điện thoại không hợp lệ" 
        })
    }
    if(!body.soDienThoai){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Địa chỉ không hợp lệ" 
        })
    }
   
    //bước 3: Gọi Model tạo dữ liệu
    const newOrder = {
        kichCo: body.kichCo,
        duongKinh: body.duongKinh,
        suon: body.suon,
        salad: body.salad,
        loaiPizza: body.loaiPizza,
        idVourcher: body.idVourcher,
        idLoaiNuocUong: body.idLoaiNuocUong,
        soLuongNuoc: body.soLuongNuoc,
        hoTen: body.hoTen,
        thanhTien: body.thanhTien,
        email: body.thanhTien,
        soDienThoai: body.soDienThoai,
        diaChi: body.diaChi,
        loiNhan: body.loiNhan,
        trangThai: body.trangThai
    };
    //bước 4: trả về kết quả 
    orderModel.findByIdAndUpdate(orderId,newOrder,{new:true}, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update order successfully", 
            data: data
        })
    })
}
const deleteOrderById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete order successfully"
        })
    })
}
const postOrderByUser = (request, response) => {
    //lấy dữ liều đầu vào query email
    let emailQuery = request.query.email;
    let body = request.body;
    orderModel.count().exec((errorCount, countOrder) => {
        let randomOrder = Math.floor(Math.random * countOrder);

        orderModel.findOne().skip(randomOrder).exec((errorFindOrder, findOrder) =>
        {
            console.log(findOrder);
            userModel.findOne({email: emailQuery}, (errorFindUser, userExit) => {
                if (errorFindUser) {
                    return response.status(500).json({
                        status: "Error 500: Internal server error",
                        message: errorFindUser.message
                    })
                } else {
                    if(userExit){
                        //nếu user có tồn tại , lấy id của user đó để tạo Order. Ngược lại nếu chưa tồn tại cần tạo 1 user mới và lấy id của user mới đó để tạo Orde
                        orderModel.create({
                            pizzaSize: findOrder.pizzaSize,
                            pizzaType: findOrder.pizzaType,
                            status: findOrder.status
                        }, (errorCreateOrder, createOrderData) => {
                            if (errorCreateOrder) {
                                return response.status(500).json({
                                    status: "Internal server error",
                                    message: error.message
                                })
                            }
                            // Thêm ID của order mới vào mảng order của user đã chọn
                            userModel.findAndUpdate({email: emailQuery}, 
                                    {$push: {
                                        order: createOrderData._id
                                    }
                                }, (errorFindUserAndUpdate, dataFindUserAndUpdate) => {
                                    if(errorFindUserAndUpdate){
                                        return response.status(500).json({
                                            status: "Internal server error",
                                            message: errorFindUserAndUpdate.message
                                        })
                                    }else{
                                        return response.status(201).json({
                                            status: "Create Order Successfully",
                                            data: data
                                        })
                                    }
                                }
                            )
                        })
                    }else{
                        //Ngược lại nếu chưa tồn tại cần tạo 1 user mới và lấy id của user mới đó để tạo Order
                        userModel.create(
                            { fullName: body.bodyFullName,
                            email: body.bodyEmail,
                            address: body.bodyAddress,
                            phone: body.bodyPhone }, (errorCreateUser, dataCreateUser) => {
                            if(errorCreateUser){
                                return response.status(500).json({
                                    status: "Internal server error",
                                    message: errorCreateUser.message
                                })
                            }else{
                                //tạo order mới
                                // Thêm ID của order mới vào mảng order của user đã chọn
                                userModel.findByIdAndUpdate(dataCreateUser._id, 
                                        {$push: {
                                            orders: findOrder._id
                                        }
                                    }, (errorFindUserAndUpdate, dataFindUserAndUpdate) => {
                                        if(errorFindUserAndUpdate){
                                            return response.status(500).json({
                                                status: "Internal server error",
                                                message: errorFindUserAndUpdate.message
                                            })
                                        }else{
                                                userModel.findById(dataCreateUser._id)
                                                    .populate("orders")
                                                    .exec((error, data) => {
                                                    if (error) {
                                                        return response.status(500).json({
                                                            status: "Internal server error",
                                                            message: error.message
                                                        })
                                                    }
                                        
                                                    return response.status(200).json({
                                                        status: "Create random orders of user successfully",
                                                        data: data
                                                })
                                            })
                                        }
                                    }
                                )       
                            } 
                        });
                    }
                }
            })
        })
    }
)}

module.exports = {
    getAllOrder,
    createOrder,
    getOrderByID,
    updateOrderByID,
    deleteOrderById,
    postOrderByUser,
}