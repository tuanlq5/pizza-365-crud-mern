//import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose 
const Schema  = mongoose.Schema;

//khởi tạo instance drink Schema từ class Schema 
const drinkSchema = new Schema({
    maNuocUong:{
        type: String,
        require: true,
        unique: true,
    },
    tenNuocUong: {
        type: String,
        require: true,
    },
    donGia:{
        type: Number,
        require:true,
    }
}, {
    timestamps: true
}, { 
    versionKey: false 
})
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Drink", drinkSchema )