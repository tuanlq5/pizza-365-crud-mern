//import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose 
const Schema  = mongoose.Schema;

//khởi tạo instance voucher Schema từ class Schema 
const voucherSchema = new Schema({
    maVoucher:{
        type: String,
        require: true,
        unique: true
    },
    phanTramGiamGia:{
        type: Number,
        require: true
    },
    ghiChu:{
        type: String,
        require: true
    }
}, {
    timestamps: true
}, { 
    versionKey: false 
})
//biên dịch course model từ voucherSchema
module.exports = mongoose.model("Voucher", voucherSchema )