// khai báo thư viện express js
const express = require("express");

//khai báo router app
const router = express.Router();

//Import drink controller
const  drinkController = require("../controller/drinkController");

router.post("/drinks", drinkController.createDrink);

router.get("/devcamp-pizza365/drinks", drinkController.getAllDrink);

router.get("/drinks/:drinkId" , drinkController.getDrinkById);

router.put("/drinks/:drinkId", drinkController.updateDrinkById);

router.delete("/drinks/:drinkId", drinkController.deleteDrinkById);

module.exports = router;