// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

//Import review controller 
const userController = require("../controller/userController");


router.get("/user", userController.getAllUser)

router.post("/user", userController.createUser );

router.get("/user/:userId", userController.getUserByID);

router.put("/user/:userId", userController.updateUserByID);

router.delete("/user/:userId", userController.deleteUserById);

module.exports = router;